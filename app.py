from flask import Flask, jsonify, request, url_for, redirect, render_template 
import pickle
import spacy
import json
import spacy.displacy as displacy
from flaskext.markdown import Markdown

# Load English tokenizer, tagger, parser, NER and word vectors
nlp = spacy.load("en_core_web_sm")


app = Flask(__name__, template_folder='template')
Markdown(app)


@app.route('/')
def index():
	return render_template('NER.html')

@app.route('/predict', methods=['POST', 'GET'])
def predict():
	data = request.form['NERtext']
	doc = nlp(data)
	HTML = displacy.render(doc, style="ent")
	
	return render_template('htmldata.html', html=HTML)









	# printing data output



	# end printing output
